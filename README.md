# Playback

*WIP*

## Installation

From source:

```sh
$ go get github.com/wabarc/playback
```

From [gobinaries.com](https://gobinaries.com):

```sh
$ curl -sf https://gobinaries.com/wabarc/playback | sh
```

From [releases](https://github.com/wabarc/playback/releases)

## Usage

## F.A.Q

### GitHub rest API rate limit?

Set personal access token with `PLAYBACK_GITHUB_PAT` environment variable to increase rate limit.

## License

This software is released under the terms of the GNU General Public License v3.0. See the [LICENSE](https://github.com/wabarc/playback/blob/main/LICENSE) file for details.
